﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class Warp : MonoBehaviour {

    //Objeto del punto de aparicion
    public GameObject target;

    //Objeto del mapa de destino
    public GameObject targetMap;


    public GameObject placeName;

    private void Awake()
    {
        //Throws Exception si no hay recibido target
        Assert.IsNotNull(target);

        GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;

        Assert.IsNotNull(targetMap);

        placeName = GameObject.FindGameObjectWithTag("Place");

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.transform.position = target.transform.GetChild(0).transform.position;
            if (SceneManager.GetActiveScene().name == "Main")
            {
                Camera.main.GetComponent<CameraControl>().SetBound(targetMap);
                StartCoroutine(placeName.GetComponent<PlaceName>().ShowPlaces(targetMap.name));
            }
        }
    }


}
