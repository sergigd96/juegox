﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMainMenu : MonoBehaviour {

    private float aspectRatio;

    private float tLX, tLY, bRX, bRY;

    public GameObject map;

    // Use this for initialization
    void Start () {
        // Script del propio Tiled para acceder a sus propiedades
        Tiled2Unity.TiledMap config = map.GetComponent<Tiled2Unity.TiledMap>();

        //Almacenar dinamicamente el tamaño de la camara
        float cameraSize = Camera.main.orthographicSize;

        // Formulación para delimitar, según el tamaño del mapa, la camara
        tLX = map.transform.position.x + cameraSize * aspectRatio;
        tLY = map.transform.position.y - cameraSize;
        bRX = map.transform.position.x + config.NumTilesWide - cameraSize * aspectRatio;
        bRY = map.transform.position.y - config.NumTilesHigh + cameraSize;
    }

	private void Awake()
    {
        aspectRatio = Camera.main.aspect;
    }


    private void LateUpdate()
    {
        // Suavizar movimiento de la camara en la Y
        float posY = Mathf.Round(-8f);
        float posX = Mathf.Round(18.5f);

        transform.position = new Vector3(
           Mathf.Clamp(posX, tLX, bRX),
           Mathf.Clamp(posY, bRY, tLY),
           transform.position.z);

    }

}
