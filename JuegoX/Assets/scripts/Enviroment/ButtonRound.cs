﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonRound : MonoBehaviour {

    RoundController roundController;
    MusicController music;
    SoundController soundController;
    EnemyGen enemyGen;
	// Use this for initialization
	void Start () {

        roundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<RoundController>();
        music = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MusicController>();
        enemyGen = GameObject.FindGameObjectWithTag("Enemies").GetComponent<EnemyGen>();
        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Player")
        {
            SendMessage("disableSprite");
            roundController.SendMessage("startRound", true);
            soundController.SendMessage("playButton");
            music.SendMessage("Fight");
            enemyGen.SendMessage("startRound");

        }
    }

    void disableSprite()
    {
        transform.GetComponent<SpriteRenderer>().enabled = false;
        transform.GetComponent<CircleCollider2D>().enabled = false;
    }

    void enableSprite()
    {
        transform.GetComponent<SpriteRenderer>().enabled = true;
        transform.GetComponent<CircleCollider2D>().enabled = true;
    }

    void normalMusic()
    {

        music.SendMessage("endRound");
    }

}
