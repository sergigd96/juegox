﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMusicController : MonoBehaviour {

    public AudioSource controller;
    public AudioClip intro;
    public AudioClip loop;

    // Use this for initialization
    void Start () {

        StartCoroutine(returnTime());
        controller.clip = loop;
        controller.loop = true;
        controller.Play();
    }

    IEnumerator returnTime()
    {
        yield return new WaitForSeconds(intro.length);
    }

    public void Stop()
    {
        controller.Stop();
    }

	
}
