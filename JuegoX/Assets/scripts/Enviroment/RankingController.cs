﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RankingController : MonoBehaviour {

    public GameObject mainMenu;

    private MenuController menuController;

    public TMP_InputField inputField;

    public GameObject rankingUi;

    private TextMeshProUGUI rankingText;

	// Use this for initialization
	void Start () {

        menuController = mainMenu.GetComponent<MenuController>();

        rankingText = rankingUi.GetComponent<TextMeshProUGUI>();

        readRanking();

	}

    private void readRanking()
    {
        if(PlayerPrefs.HasKey("Nick") && PlayerPrefs.HasKey("Round"))
        {
            rankingText.text = PlayerPrefs.GetString("Nick") + Environment.NewLine + " Ronda: " + PlayerPrefs.GetFloat("Round");
        }
    }

    // Update is called once per frame
    void Update () {

        saveRankingInfo();

	}

    public void saveRankingInfo()
    {
        if (inputField.text != "" && Input.GetButton("Submit"))
        {
            menuController.NewGame();
        }
        
    }

}
