﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RoundController : MonoBehaviour {

    // Limites del campo de batalla
    private GameObject battleArea;

    // Boton de ronda
    private GameObject buttonControl;

    private GameObject roundUI;

    private int round;

    private bool roundStart;

    // Use this for initialization
    void Awake () {
        battleArea = GameObject.FindGameObjectWithTag("BattleArea");
        buttonControl = GameObject.FindGameObjectWithTag("Button_idle");
        roundUI = GameObject.FindGameObjectWithTag("RoundUI");
        roundStart = false;
        round = 1;
	}

    void startRound(bool start)
    {
        roundUI.GetComponent<Text>().text = "Round " + round;
        roundUI.GetComponent<Text>().enabled = true;
        //Activar/desactivar limites del campo de batalla
        if (start)
        {
            roundStart = true;
            battleArea.GetComponent<EdgeCollider2D>().enabled = true;
        }
        else
        {
            roundStart = false;
            battleArea.GetComponent<EdgeCollider2D>().enabled = false;
        }
    }

    void updateRound()
    {
        round++;
        SendMessage("startRound", false);
    }

    void finishRound()
    {
        updateRound();
        buttonControl.SendMessage("enableSprite");
        buttonControl.SendMessage("normalMusic");
    }

    internal float getRound()
    {
        return round;
    }

    public bool getStart()
    {
        return roundStart;
    }
    
}
