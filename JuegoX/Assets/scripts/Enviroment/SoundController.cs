﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {

    public AudioSource controller;
    public AudioClip[] effects;

    //Reproducir sonido moneda
    void playCoin()
    {
        controller.clip = effects[0];
        controller.volume = 0.8f;
        controller.Play();
    }

    //Reproducir sonido curar
    void playHeal()
    {
        controller.clip = effects[1];
        controller.volume = 0.8f;
        controller.Play();
    }

    //Reproducir sonido daño player
    void playHitPlayer()
    {
        controller.clip = effects[2];
        controller.volume = 0.8f;
        controller.Play();
    }

    //Reproducir sonido muerte player
    void playDeadPlayer()
    {
        controller.clip = effects[3];
        controller.volume = 0.8f;
        controller.Play();
    }

    //Reproducir sonido golpe enemigo
    void playHitEnemy()
    {
        controller.clip = effects[4];
        controller.volume = 0.8f;
        controller.Play();
    }

    //Reproducir sonido golpe "escenario"
    void playHitScene()
    {
        controller.clip = effects[5];
        controller.volume = 0.8f;
        controller.Play();
    }

    // Reproducir sonido golpe Ambrosio
    void playHitPingu()
    {
        controller.clip = effects[6];
        controller.volume = 0.8f;
        controller.Play();
    }

    // Reproducir sonido golpe Isaac
    void playHitIsaac()
    {
        controller.clip = effects[Random.Range(7, 9)];
        controller.volume = 0.8f;
        controller.Play();
    }

    // Reproducir sonido pulsar boton
    void playButton()
    {
        controller.clip = effects[9];
        controller.volume = 0.8f;
        controller.Play();
    }

    // Reproducir sonido al matar al enemigo
    void playEnemyDeath()
    {
        controller.clip = effects[Random.Range(11, 13)];
        controller.volume = 0.8f;
        controller.Play();
    }

    // Reproducir sonido al comprar un objeto
    void playBuy()
    {
        controller.clip = effects[14];
        controller.volume = 0.25f;
        controller.Play();
    }


    void playEndRound()
    {
        controller.clip = effects[15];
        controller.volume = 1f;
        controller.Play();
    }

    // Reproducir sonido al eliminar la hierba
    void playHitGrass()
    {
        controller.clip = effects[16];
        controller.volume = 0.8f;
        controller.Play();
    }

    // Reproducir sonido al eliminar el jarrón
    void playHitJar()
    {
        controller.clip = effects[17];
        controller.volume = 0.8f;
        controller.Play();
    }

    void playHitBone()
    {
        controller.clip = effects[18];
        controller.volume = 0.8f;
        controller.Play();
    }
}
