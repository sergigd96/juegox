﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {

    public AudioSource controller;
    public AudioClip[] music;

    // Estado easterEgg
    private bool easterEgg = false;

    public void ChangeMusic(string place)
    {
        if (!easterEgg)
        {
            switch (place)
            {
                case "Arena":
                    controller.Stop();
                    controller.clip = music[0];
                    controller.loop = true;
                    controller.volume = 1f;
                    controller.Play();
                    break;

                case "Outside Palace":
                    controller.Stop();
                    controller.clip = music[2];
                    controller.loop = true;
                    controller.volume = 1f;
                    controller.Play();
                    break;

                case "Ambrosio's Cave":
                    controller.Stop();
                    controller.clip = music[3];
                    controller.loop = true;
                    controller.volume = 1f;
                    controller.Play();
                    break;

                case "Forest":
                    controller.Stop();
                    controller.clip = music[5];
                    controller.loop = true;
                    controller.volume = 1f;
                    controller.Play();
                    break;
            }
        }
    }

    public void GameOver()
    {
        controller.Stop();
        controller.clip = music[1];
        controller.volume = 1f;
        controller.loop = true;
        controller.Play();
    }

    public void IsaacEasterEgg()
    {

        easterEgg = true;
        StartCoroutine(playEngineSound());
        

    }

    private IEnumerator playEngineSound()
    {
        controller.Stop();
        controller.clip = music[4];
        controller.volume = 1f;
        controller.loop = false;
        controller.Play();
        yield return new WaitForSeconds(music[4].length);
        easterEgg = false;
    }

    public void Fight()
    {
        if (!easterEgg)
        {
            int index = Random.Range(6, 10);
            controller.Stop();
            controller.clip = music[index];
            controller.volume = 1f;
            controller.loop = true;
            controller.Play();
        }

    }

    public void endRound()
    {
        if (!easterEgg)
        {
            controller.Stop();
            controller.clip = music[0];
            controller.loop = true;
            controller.volume = 1f;
            controller.Play();
        }
    }
}
