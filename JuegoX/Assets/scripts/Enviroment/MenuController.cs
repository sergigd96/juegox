﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using System;

public class MenuController : MonoBehaviour
{
    public TMP_InputField inputField;
    public GameObject mainCamera;
    private bool reseted;

    void Awake()
    {
        reseted = false;
    }

    public void NewGame()
    {
        if (inputField.text != "")
        {
            loadScreen();
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

    void Update()
    {
        if (Input.GetButton("Esc"))
        {
            Application.Quit();
        }

        if(Input.GetKeyDown(KeyCode.LeftControl))
        {
            if (Input.GetKey(KeyCode.R))
            {
                resetRankings();
            }
        }

    }

    private void resetRankings()
    {
        if (!reseted)
        {
            PlayerPrefs.DeleteAll();
            GetComponent<SoundController>().SendMessage("playCoin");
            reseted = true;
        }

    }

    private void loadScreen()
    {
        mainCamera.GetComponent<MainMusicController>().Stop();
        PlayerPrefs.SetString("TemporalNick", inputField.text);
        SceneManager.LoadScene("LoadScreen");
    }
}
