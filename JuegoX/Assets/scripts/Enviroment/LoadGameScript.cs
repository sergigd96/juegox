﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadGameScript : MonoBehaviour {

    public Text loadingText;

	// Use this for initialization
	void Start () {
        StartCoroutine(LoadGameAsync());
	}

    private IEnumerator LoadGameAsync()
    {
        AsyncOperation asyncScene = SceneManager.LoadSceneAsync("Main", LoadSceneMode.Single);
        asyncScene.allowSceneActivation = false;
        while (!asyncScene.isDone)
        {
            if (asyncScene.progress == 0.9f)
            {
                loadingText.text = "Press Any Button";
                if (Input.anyKey)
                {
                    asyncScene.allowSceneActivation = true;
                }
            }
            yield return null;
        }



    }

}
