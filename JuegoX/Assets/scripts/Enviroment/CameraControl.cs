﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraControl : MonoBehaviour {


    private GameObject player;
    public Transform target;
    private float aspectRatio;
    private RoundController roundController;

    public float smoothTime = 0.2f;

    // Limites del mapa
    private float tLX, tLY, bRX, bRY;

    Vector2 velocity;
    private GameObject textInfo;

    void Start()
    {
        // Get texto Info al jugador
        textInfo = GameObject.FindGameObjectWithTag("Info");
        roundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<RoundController>();

    }

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform;

        // Variable que almacena la división del AspectRatio
        aspectRatio = Camera.main.aspect;

    }

    private void Update()
    {
        // Permitir cerrar juego al presionar escape
        quitGame();

        // Repetir juego pulsando R
        restartGame();

        // Game Over Mensaje
        gameOver();
    }

    private void gameOver()
    {
        textInfo.SetActive(false);
        if (target == null)
        {
            textInfo.SetActive(true);
        }
    }

    private void LateUpdate()
    {
        if (target != null)
        {
            // Suavizar movimiento de la camara en la X
            float posX = Mathf.Round(
                 Mathf.SmoothDamp(
                    transform.position.x,
                    target.position.x,
                    ref velocity.x,
                    smoothTime)
                 // Truco para redondear los floats
                 * 100) / 100;

            // Suavizar movimiento de la camara en la Y
            float posY = Mathf.Round(
                 Mathf.SmoothDamp(
                    transform.position.y,
                    target.position.y,
                    ref velocity.y,
                    smoothTime)
                 * 100) / 100;

            transform.position = new Vector3(
                Mathf.Clamp(posX, tLX, bRX),
                Mathf.Clamp(posY, bRY, tLY),
                transform.position.z);
        }
    }

    public void SetBound(GameObject map)
    {
        if (target != null)
        {
            // Script del propio Tiled para acceder a sus propiedades
            Tiled2Unity.TiledMap config = map.GetComponent<Tiled2Unity.TiledMap>();

            //Almacenar dinamicamente el tamaño de la camara
            float cameraSize = Camera.main.orthographicSize;

            // Formulación para delimitar, según el tamaño del mapa, la camara
            tLX = map.transform.position.x + cameraSize * aspectRatio;
            tLY = map.transform.position.y - cameraSize;
            bRX = map.transform.position.x + config.NumTilesWide - cameraSize * aspectRatio;
            bRY = map.transform.position.y - config.NumTilesHigh + cameraSize;

            FastMove();
        }
    }


    public void FastMove()
    {
        if (target != null)
        {
            transform.position = new Vector3(
            target.position.x,
            target.position.y,
            transform.position.z
        );
        }
    }
    

    private void restartGame()
    {
        if (Input.GetButton("Reset") && player == null)
        {
            SceneManager.LoadScene("Main", LoadSceneMode.Single);
        }
    }

    private void quitGame()
    {
        if (Input.GetButton("Esc") && player == null)
        {
            bestPlayer();
            SceneManager.LoadScene(0);
        } else if (Input.GetButton("Esc"))
        {
            SceneManager.LoadScene(0);
        }
    }

    private void bestPlayer()
    {
        if (roundController.getRound() > PlayerPrefs.GetFloat("Round"))
        {
            PlayerPrefs.SetString("Nick", PlayerPrefs.GetString("TemporalNick"));
            PlayerPrefs.SetFloat("Round", roundController.getRound());
            PlayerPrefs.Save();
        }
    }

    public void DeleteAll()
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("PickUps"))
        {
            Destroy(obj);
        }
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
            DeleteAll();
    }
}
