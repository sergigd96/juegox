﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGen : MonoBehaviour {

    RoundController roundController;

    public GameObject[] Spawners;

    public GameObject[] Enemies;

    private int index, enemy;

    private int defeated;
    private int maxNumEnemies;
    private int actualNumEnemies;

    private void Awake()
    {
        
    }

    // Use this for initialization
    void Start () {

        roundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<RoundController>();
        // Declarar numero de enemigos y maximos por ronda
        maxNumEnemies = 3;
        actualNumEnemies = 0;
}

    private void comprobarRonda()
    {
        if (defeated == maxNumEnemies)
        {
            roundController.SendMessage("finishRound");
            maxNumEnemies++;
        }
    }

    void startRound()
    {
        defeated = 0;
        actualNumEnemies = 0;
        for (int i = actualNumEnemies; i < maxNumEnemies; i++)
        {
            Invoke("spawnEnemies", 0.5f);
        }
    }

    void killEnemy()
    {
        actualNumEnemies--;
        defeated++;
        comprobarRonda();
    }

    void spawnEnemies()
    {
        probabilidadEnemies();
        index = Random.Range(0, 12);
        Instantiate(Enemies[enemy], Spawners[index].transform.position, Quaternion.identity);
    }

    private void probabilidadEnemies()
    {
        enemy = Random.Range(0, 100);
        if (enemy <= 60)
        {
            enemy = 1;
        }
        else enemy = 0;

    }
}
