﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsUI : MonoBehaviour {

    private GameObject player;

    private PlayerStats stats;



	// Use this for initialization
	void Start () {

        player = GameObject.FindGameObjectWithTag("Player");
        stats = player.GetComponent<PlayerStats>();

        // Texto Attack
        transform.GetChild(0).GetComponent<Text>().text = "Atk: " + stats.Attack;

        // Texto Speed
        transform.GetChild(1).GetComponent<Text>().text = "Spd: " + stats.Speed;

        // Texto Speed
        transform.GetChild(2).GetComponent<Text>().text = "Range: "+ stats.Range;



    }

    void updateAttack()
    {
        // Texto Attack
        transform.GetChild(1).GetComponent<Text>().text = "Atk: " + stats.Attack;
    }

    void updateSpeed()
    {
        // Texto Speed
        transform.GetChild(1).GetComponent<Text>().text = "Spd: " + stats.Speed;
    }

    void updateRange()
    {
        // Texto Range
        transform.GetChild(2).GetComponent<Text>().text = "Range" + stats.Range;

    }

}
