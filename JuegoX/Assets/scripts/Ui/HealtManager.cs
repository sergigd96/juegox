﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealtManager : MonoBehaviour {

    private GameObject player;

    private PlayerStats playerStats;

    private int maxHeartAmount = 12;
    private int startHearts = 3;
    private float actualHp;
    private int healthPerHeart = 2;

    public Image[] healthImages;
    public Sprite[] healthSprites;

	// Use this for initialization
	void Start () {

        player = GameObject.FindGameObjectWithTag("Player");
        playerStats = player.GetComponent<PlayerStats>();
        actualHp = playerStats.Hp;
        checkHealthAmount();
	}
	

    public void updateLife() {
        if (playerStats.Hp <= playerStats.MaxHealth)
        {
            actualHp = playerStats.Hp;
            startHearts = (int) playerStats.MaxHealthActual/2;
            updateHearts();
        }
    }


    void checkHealthAmount()
    {
        for(int i = 0; i < maxHeartAmount; i++)
        {
            //Eliminar corazones en el UI
            if(startHearts <= i) healthImages[i].enabled = false;
            //Insert corazones en el UI
            else healthImages[i].enabled = true;
        }
    }

    void updateHearts()
    {
        bool empty = false;
        int i = 0;

        foreach (Image img in healthImages)
        {
            // Corazón vacio
            if (empty) img.sprite = healthSprites[0];
            else
            {
                i++;
                // Corazón Lleno
                if (actualHp >= i * healthPerHeart) img.sprite = healthSprites[healthSprites.Length-1];
                // Corazón partido
                else
                {
                    int currentHeartHealth = (int)(healthPerHeart-(healthPerHeart*i - actualHp));
                    int healthPerImage = healthPerHeart / (healthSprites.Length-1);
                    int imageIndex = (int) currentHeartHealth / healthPerImage;
                    img.sprite = healthSprites[imageIndex];
                    empty = true;

                }
            }
        }

    }


    void addHeart()
    {
        if (startHearts < maxHeartAmount)
        {
            playerStats.MaxHealthActual += 2;
            playerStats.Hp += 2;
            updateLife();
            checkHealthAmount();
        }
    }

}
