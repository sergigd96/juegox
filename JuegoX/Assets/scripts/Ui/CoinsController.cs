﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsController : MonoBehaviour {

    GameObject coinsUI;

    PlayerStats playerStats;

	// Use this for initialization
	void Start () {

        coinsUI = GameObject.FindGameObjectWithTag("Coins");
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
    }
	

    public void UpdateCoins()
    {
        coinsUI.GetComponent<Text>().text = ""+playerStats.coins;
    }

}
