﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjFamiliar : MonoBehaviour {

    private SoundController soundController;

    public GameObject prefabFamiliar;

    private CoinsController coins;

    // Use this for initialization
    void Start()
    {

        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>(); ;
        coins = GameObject.FindGameObjectWithTag("CoinsUI").GetComponent<CoinsController>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (collision.GetComponent<PlayerStats>().Coins >= 10)
            {
                buyFamiliar(collision);
            }
        }
    }

    private void buyFamiliar(Collider2D collision)
    {
        collision.GetComponent<PlayerStats>().Coins += -10;
        coins.SendMessage("UpdateCoins");
        soundController.SendMessage("playBuy");
        collision.GetComponent<Animator>().SetTrigger("getItem");
        Instantiate(prefabFamiliar, collision.transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    private void OnGUI()
    {

        Vector2 pos = Camera.main.WorldToScreenPoint(transform.position);

        GUI.Box(
           new Rect(
               pos.x - 15,
               Screen.height - pos.y + 20,
               30,
               20
            ), "10");

    }
}
