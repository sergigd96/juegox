﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FollowerAI : MonoBehaviour {

    private Transform player;
    public float speed = 3f;
    private GameObject enemy;
    private bool attacking;
    private RoundController roundController;
    private Slider roundUI;

    [Tooltip("Prefab del hueso que se disparará")]
    public GameObject bonePrefab;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        roundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<RoundController>();
    }

    void Update()
    { 

        movementToPlayer();

        setEnemy();

    }

    private void setEnemy()
    {
        if (roundController.getStart()) {
            if (!enemy)
            {
                enemy = GameObject.FindGameObjectWithTag("Enemy");
            }
            else
            {
                if (!attacking) StartCoroutine(Attack(4f));
            }
        }
    }


    IEnumerator Attack(float seconds)
    {
        attacking = true;
        // Si tenemos objetivo y el prefab se crea un hueso
        if (bonePrefab != null)
        {
            Instantiate(bonePrefab, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(seconds);
        }
        attacking = false;
    }


    void movementToPlayer()
    {
        //Rotación de follower para apuntar al Player
        transform.LookAt(player.position);
        transform.Rotate(new Vector3(0, -90, 0), Space.Self);


        //Si la distancia del follower con el player es superior a 1, se moverá hacia la dirección del player
        if (Vector3.Distance(transform.position, player.position) > 1f)
        {
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }


        //Si la distancia del follower con el player es superior a 15, se teletransportará al lado del player
        if (Vector3.Distance(transform.position, player.position) > 15f)
        {
            transform.position = new Vector3(player.position.x - 1, player.position.y, 0);
        }
    }
}
