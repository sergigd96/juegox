﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bone : MonoBehaviour {

    private GameObject enemy;
    private Rigidbody2D rb2d;
    private float speed;
    private Vector3 target, dir;
    private PlayerStats playerStats;
    private SoundController soundController;

    // Use this for initialization
    void Start () {
        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        rb2d = GetComponent<Rigidbody2D>();
        speed = 4.5f;
        if (enemy != null)
        {
            target = enemy.transform.position;
            dir = (target - transform.position).normalized;
        }

    }
	
    void FixedUpdate()
    {
        // Si hay un objetivo movemos el hueso hacia su posición
        if (target != Vector3.zero)
        {
            rb2d.MovePosition(transform.position + (dir * speed) * Time.deltaTime);
        }
    }

    IEnumerator OnTriggerEnter2D(Collider2D col)
    {
        // Si chocamos contra el jugador o un ataque la borramos
        if (col.transform.tag == "Enemy")
        {
            col.SendMessage("attacked", playerStats.attack/2);
            soundController.SendMessage("playHitBone");
            Destroy(gameObject);
        }
        else
        {
            yield return new WaitForSeconds(3.5f);
            Destroy(gameObject);
        }
    }

}
