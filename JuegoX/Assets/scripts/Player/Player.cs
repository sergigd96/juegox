﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Player : MonoBehaviour
{

    private PlayerStats playerStats;
    private Vector2 move;
    private Vector2 attack;
    private Rigidbody2D rb2d;
    private Animator anim;
    private bool attacking;
    private AnimatorStateInfo stateInfo;
    private bool attackPrevent;

    private CircleCollider2D attackCollider;

    public GameObject initialMap;
    public GameObject slashPrefab;

    private Aura aura;

    private float lastTime;


    private MusicController musicController;
    private SoundController soundController;
    public HealtManager manager;

    void Awake()
    {
        Assert.IsNotNull(initialMap);
        Assert.IsNotNull(slashPrefab);
    }

    // Use this for initialization
    void Start()
    {

        //Inizilizar los stats del jugador
        playerStats = GetComponent<PlayerStats>();

        //Recoger el controlador de sonido y de música
        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();
        musicController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MusicController>();

        //Get Animator y Rigidbody del Player
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

        //Get collider de ataque
        attackCollider = transform.GetChild(0).GetComponent<CircleCollider2D>();
        //Desactivar el collider de ataque
        attackCollider.enabled = false;

        //Get mapa asociado a la camara
        Camera.main.GetComponent<CameraControl>().SetBound(initialMap);

        //Variable para controlar el ataque
        attackPrevent = false;

        //Get componente Aura del Player
        aura = transform.GetChild(1).GetComponent<Aura>();

    }

    // Update is called once per frame
    void Update()
    {

        // Movimiento del personaje
        movement();

        // Ataque del personaje (Get Axis)
        playerAttacks();

        // Ataque normal (Con espada)
        swordAttack();

        // Ataque distancia
        slashAttack();

    }

    void FixedUpdate()
    {
        rb2d.MovePosition(rb2d.position + move * playerStats.Speed * Time.deltaTime);
    }


    private void swordAttack()
    {
        //Esperar unos segundos antes de volver a atacar
        StartCoroutine(EnableAttackAfter(5f));

        if (attack != Vector2.zero && !attacking)
        {
            //Get dirección del ataque
            anim.SetFloat("attackY", attack.y);
            anim.SetFloat("attackX", attack.x);
            anim.SetBool("attacking", true);

            //Get dirección del collider del ataque
            attackCollider.offset = new Vector2(attack.x / 2, attack.y / 2);
            //Esperar unos segundos antes de volver a atacar (Despues del slash)
            StartCoroutine(EnableAttackAfter(0.4f));

        }
        else
        {
            anim.SetBool("attacking", false);
        }

        if (attacking && !attackPrevent)
        {
            //Activar/Desactivar Collider según el 1/3 de la duración de la animación
            float playbackTime = stateInfo.normalizedTime;
            if (playbackTime > 0.33 && playbackTime < 0.66)
                attackCollider.enabled = true;
            else
                attackCollider.enabled = false;

            //Esperar unos segundos antes de volver a atacar
            StartCoroutine(EnableAttackAfter(5f));
        }

    }

    private void movement()
    {
        // Movimiento del player
        move = new Vector2(
           Input.GetAxisRaw("Horizontal"),
           Input.GetAxisRaw("Vertical"));

        // Condición de movimiento
        if (move != Vector2.zero)
        {
            anim.SetBool("moving", true);
        }
        else
        {
            anim.SetBool("moving", false);
        }

    }

    private void playerAttacks()
    {
        // Ataque del player
        attack = new Vector2(
            Input.GetAxisRaw("AttackX"),
            Input.GetAxisRaw("AttackY"));

        //Animación de ataque
        anim.SetFloat("moveX", move.x);
        anim.SetFloat("moveY", move.y);

        // Obtener info de la animación de ataque
        stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        attacking = stateInfo.IsName("Player_Attack");
    }


    private void slashAttack()
    {
        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        bool charge = stateInfo.IsName("Slash_Attack");

        if (Input.GetButtonDown("Space"))
        {
            anim.SetBool("charge", true);
            aura.AuraStart();
        }
        else if (Input.GetButtonUp("Space"))
        {
            anim.SetBool("attacking", true);

            if (aura.IsLoaded())
            {
                float angle = Mathf.Atan2(
                    anim.GetFloat("attackY"),
                    anim.GetFloat("attackX")) * Mathf.Rad2Deg;

                GameObject slashAtt = Instantiate(
                    slashPrefab, transform.position,
                    Quaternion.AngleAxis(angle, Vector3.forward)
                );

                Slash slash = slashAtt.GetComponent<Slash>();
                slash.mov.x = anim.GetFloat("attackX");
                slash.mov.y = anim.GetFloat("attackY");
            }
            aura.AuraStop();
            anim.SetBool("charge", false);

        }

        if (charge)
        {
            attackPrevent = true;
        }

    }

    IEnumerator EnableAttackAfter(float secs)
    {
        yield return new WaitForSeconds(secs);
        attackPrevent = false;
    }

    ///--- Gestión del ataque del enemigo, restamos vida
    public void attacked(float attack)
    {
        playerStats.Hp -= attack;
        soundController.SendMessage("playHitPlayer");
        manager.updateLife();
        if (playerStats.Hp <= 0)
        {
            soundController.SendMessage("playDeadPlayer");
            Destroy(gameObject);
        };

    }

    ///--- Gestión de curar al player
    public void heal()
    {
        playerStats.Hp += 1f;
        soundController.SendMessage("playHeal");
        manager.SendMessage("updateLife");
    }

    void OnDestroy()
    {
       musicController.SendMessage("GameOver");
    }
}