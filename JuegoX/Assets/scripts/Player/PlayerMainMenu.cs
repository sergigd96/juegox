﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMainMenu : MonoBehaviour
{

    private Vector2 move;
    private Rigidbody2D rb2d;
    private Animator anim;
    private int speed;

    // Use this for initialization
    void Start()
    {

        //Get Animator y Rigidbody del Player
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        move = new Vector2(1, 0);
        speed = Random.Range(4, 10);

    }

    void FixedUpdate()
    {
        rb2d.MovePosition(rb2d.position + move * speed * Time.deltaTime);
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("moving", true);
        anim.SetFloat("moveX", 1);
        anim.SetFloat("moveY", 0);

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        speed = Random.Range(4, 10);
    }
}
