﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slash : MonoBehaviour {

    private GameObject player;
    private PlayerStats playerStats;

    private SoundController soundController;

    [Tooltip("Segundos antes de destruir el Slash")]
    public float waitBeforeDestroy;

    [HideInInspector]
    public Vector2 mov;

    public float speed;

    public float range;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerStats = player.GetComponent<PlayerStats>();
        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();
        speed = playerStats.Speed;
        range = playerStats.Range;
        print(playerStats.Slash);
    }

    void Update()
    {
        transform.position += new Vector3(mov.x, mov.y, 0) * speed * Time.deltaTime;
    }

    IEnumerator OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Grass")
        {
            soundController.SendMessage("playHitGrass");
            yield return new WaitForSeconds(waitBeforeDestroy);
            Destroy(gameObject);
        } else if (col.tag == "Jar")
        {
            soundController.SendMessage("playHitGrass");
            yield return new WaitForSeconds(waitBeforeDestroy);
            Destroy(gameObject);
        }
        else if (col.tag != "Player" && col.tag != "Attack" && col.tag != "Scene" && col.tag != "BattleArea" && col.tag != "Button_idle" && col.tag != "Coin" && col.tag != "Heart" && col.tag != "Obj")
        {
            if (col.tag == "Enemy")
            {
                soundController.SendMessage("playHitEnemy");
                col.SendMessage("attacked", playerStats.Slash);
            } else soundController.SendMessage("playHitScene");
            Destroy(gameObject);
        }
        else
        {
            yield return new WaitForSeconds(playerStats.Range);
            Destroy(gameObject);
        }

    }

}
