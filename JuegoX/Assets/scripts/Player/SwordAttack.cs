﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAttack : MonoBehaviour {

    private GameObject player;

    private float attack;
    private bool easterEgg = false;

    private PlayerStats playerStats;

    private SoundController soundController;

    private MusicController musicController;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerStats = player.GetComponent<PlayerStats>();
        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();
        musicController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MusicController>();
        attack = playerStats.Attack;
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Enemy")
        {
            soundController.SendMessage("playHitEnemy");
            col.SendMessage("attacked", attack);
        }
        else if(col.tag == "Ambrosio")
        {
            soundController.SendMessage("playHitPingu");
            // Añadir funcion
        }
        else if(col.tag == "Isaac")
        {
            soundController.SendMessage("playHitIsaac");
            if (!easterEgg)
            {
                musicController.SendMessage("IsaacEasterEgg");
                easterEgg = true;
            }
        }
        else if(col.tag == "Grass")
        {
            soundController.SendMessage("playHitGrass");
        }
        else if(col.tag == "Jar")
        {
            soundController.SendMessage("playHitJar");
        }

    }
}
