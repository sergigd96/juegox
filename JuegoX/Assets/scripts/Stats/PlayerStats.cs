﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {
    // Vida del Player
    [Tooltip("Puntos de vida del player")]
    public float hp = 6f;

    // Vida máxima del Player
    public float maxHealth = 24f;

    // Vida máxima actual
    public float maxHealthActual = 6f;

    // Ataque del Player
    [Tooltip("Puntos de ataque del player")]
    public float attack = 4.5f;

    // Ataque del Player con Slash
    [Tooltip("Puntos de ataque del player con Slash")]
    public float slash;

    // Rango de alcance del Slash
    [Tooltip("Rango de alcance del Slash")]
    public float range = 0.5f;

    // Speed del Player
    [Tooltip("Puntos de velocidad del player")]
    public float speed = 6f;

    // Coins del Player
    [Tooltip("Coins del player")]
    public int coins = 0;

    public PlayerStats()
    {
    }

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }

    public float Attack
    {
        get
        {
            return attack;
        }

        set
        {
            attack = value;
        }
    }

    public float Hp
    {
        get
        {
            return hp;
        }

        set
        {
            hp = value;
        }
    }

    public float Slash
    {
        get
        {
            return attack*1.5f;
        }

        set
        {
            slash = value;
        }
    }

    public float Range
    {
        get
        {
            return range;
        }

        set
        {
            range = value;
        }
    }

    public int Coins
    {
        get
        {
            return coins;
        }

        set
        {
            coins = value;
        }
    }

    public float MaxHealth
    {
        get
        {
            return maxHealth;
        }

        set
        {
            maxHealth = value;
        }
    }

    public float MaxHealthActual
    {
        get
        {
            return maxHealthActual;
        }

        set
        {
            maxHealthActual = value;
        }
    }
}
