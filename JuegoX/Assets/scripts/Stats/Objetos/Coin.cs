﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{

    PlayerStats player;
    CoinsController coinsController;
    SoundController soundController;

    int value;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        coinsController = GameObject.FindGameObjectWithTag("CoinsUI").GetComponent<CoinsController>();
        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();
        value = (int) Random.Range(1, 4);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            soundController.SendMessage("playCoin");
            // La suma actual con lo conseguido no supere el limite
            if (player.coins + value <= 99)
            {
                // Limite monedas a 99
                if (player.coins < 99)
                {
                    player.coins += value;
                    coinsController.UpdateCoins();
                }
            } else
            {
                player.coins = 99;
                coinsController.UpdateCoins();
            }
            Destroy(gameObject);
        }
    }
}
