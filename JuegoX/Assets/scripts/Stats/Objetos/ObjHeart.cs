﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjHeart : MonoBehaviour {

    private SoundController soundController;

    private SpriteRenderer alert;

    private HealtManager healt;

    private CoinsController coins;

    // Use this for initialization
    void Start () {

        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>(); ;
        healt = GameObject.FindGameObjectWithTag("HealthManager").GetComponent<HealtManager>();
        coins = GameObject.FindGameObjectWithTag("CoinsUI").GetComponent<CoinsController>();
        alert = GameObject.FindGameObjectWithTag("Ambrosio").GetComponent<SpriteRenderer>();
        alert.enabled = false;
    }


    private IEnumerator OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<PlayerStats>().Coins >= 15)
            {
                alert.enabled = true;
                buyHeart(collision.gameObject);
                yield return new WaitForSeconds(0.25f);
                alert.enabled = false;

            }
        }
    }

    private void buyHeart(GameObject collision)
    {
        collision.GetComponent<PlayerStats>().Coins += -15;
        coins.SendMessage("UpdateCoins");
        soundController.SendMessage("playBuy");
        collision.GetComponent<Animator>().SetTrigger("getItem");
        healt.SendMessage("addHeart");
    }

    private void OnGUI()
    {

        Vector2 pos = Camera.main.WorldToScreenPoint(transform.position);

        GUI.Box(
           new Rect(
               pos.x - 15,
               Screen.height - pos.y + 20,
               30,
               20
            ), "15");

    }

}
