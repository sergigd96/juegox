﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour {

    PlayerStats playerStats;
    Player player;

    SoundController soundController;
    // Use this for initialization
    void Start () {
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();
	}


    void OnCollisionEnter2D(Collision2D col)
    {
        gameObject.GetComponent<Rigidbody2D>().drag = 1.5f;
        if (col.gameObject.tag == "Player")
        {
            if (playerStats.Hp < playerStats.MaxHealthActual)
            {
                soundController.SendMessage("playHeal");
                player.heal();
                Destroy(gameObject);
            }
        }
    }


 }
