﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obj_Speed : MonoBehaviour {

    private SoundController soundController;

    private SpriteRenderer alert;

    private PlayerStats playerStats;

    private StatsUI statsUI;

    private CoinsController coins;

    // Use this for initialization
    void Start()
    {

        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>(); ;
        coins = GameObject.FindGameObjectWithTag("CoinsUI").GetComponent<CoinsController>();
        alert = GameObject.FindGameObjectWithTag("Ambrosio").GetComponent<SpriteRenderer>();
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        statsUI = GameObject.FindGameObjectWithTag("StatsUI").GetComponent<StatsUI>();
        alert.enabled = false;
    }


    private IEnumerator OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<PlayerStats>().Coins >= 15)
            {
                alert.enabled = true;
                buySpeed(collision.gameObject);
                yield return new WaitForSeconds(0.25f);
                alert.enabled = false;

            }
        }
    }

    private void buySpeed(GameObject collision)
    {
        collision.GetComponent<PlayerStats>().Coins += -15;
        coins.SendMessage("UpdateCoins");
        soundController.SendMessage("playBuy");
        collision.GetComponent<Animator>().SetTrigger("getItem");
        if (playerStats.Speed < 10)
        {
            playerStats.Speed += 0.5f;
            statsUI.SendMessage("updateSpeed");
        }
    }

    private void OnGUI()
    {

        Vector2 pos = Camera.main.WorldToScreenPoint(transform.position);

        GUI.Box(
           new Rect(
               pos.x - 15,
               Screen.height - pos.y + 20,
               30,
               20
            ), "15");

    }
}
