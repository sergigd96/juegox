﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjHeartHeal : MonoBehaviour {

    private PlayerStats playerStats;
    private Player player;
    private SpriteRenderer alert;
    private StatsUI statsUI;
    private CoinsController coins;

    SoundController soundController;
    // Use this for initialization
    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        coins = GameObject.FindGameObjectWithTag("CoinsUI").GetComponent<CoinsController>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        alert = GameObject.FindGameObjectWithTag("Ambrosio").GetComponent<SpriteRenderer>();
        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();
    }


    IEnumerator OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player" && col.gameObject.GetComponent<PlayerStats>().Coins >= 5 && playerStats.Hp < playerStats.MaxHealthActual)
        {
                alert.enabled = true;
                soundController.SendMessage("playHeal");
                buyHeal(col.gameObject);
                yield return new WaitForSeconds(0.25f);
                alert.enabled = false;
        }
    }


    private void buyHeal(GameObject collision)
    {
        collision.GetComponent<PlayerStats>().Coins += -5;
        coins.SendMessage("UpdateCoins");
        soundController.SendMessage("playBuy");
        player.heal();
    }

    private void OnGUI()
    {

        Vector2 pos = Camera.main.WorldToScreenPoint(transform.position);

        GUI.Box(
           new Rect(
               pos.x - 15,
               Screen.height - pos.y + 20,
               30,
               20
            ), "5");

    }

}
