﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour {

    // Vida del Enemigo
    [Tooltip("Puntos de vida del Enemigo")]
    public float hp = 8.75f;

    // Ataque del Enemigo
    [Tooltip("Puntos de ataque del Enemigo")]
    public float attack = 1f;

    // Speed del Enemigo
    [Tooltip("Puntos de velocidad del Enemigo")]
    public float speed = 2.5f;

    // Vision del Enemigo
    [Tooltip("Campo de visión del Enemigo")]
    public float vision = 10f;

    // Vision del Enemigo
    [Tooltip("Campo de visión del Enemigo")]
    public float radiousAttk = 0.5f;

    public EnemyStats()
    {
    }

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }

    public float Attack
    {
        get
        {
            return attack;
        }

        set
        {
            attack = value;
        }
    }

    public float Hp
    {
        get
        {
            return hp;
        }

        set
        {
            hp = value;
        }
    }

    public float RadiousAttk
    {
        get
        {
            return radiousAttk;
        }

        set
        {
            radiousAttk = value;
        }
    }

    public float Vision
    {
        get
        {
            return vision;
        }

        set
        {
            vision = value;
        }
    }
}
