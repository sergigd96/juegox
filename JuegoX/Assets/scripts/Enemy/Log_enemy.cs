﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Log_enemy : MonoBehaviour {

    private EnemyGen gen;
    private EnemyStats enemyStats;

    // Variables para gestionar el radio de visión, el de ataque, la velocidad y la vida
    public float visionRadius;
    public float attackRadius;
    private float speed;
    private float hp = 0;
    private float round;
    private float attack;
    private RoundController roundController;
    private SoundController soundController;


    // Prefabs de coins i hearts
    public GameObject coin;
    public GameObject heart;

    private AnimatorStateInfo stateInfo;

    // Variable para guardar al jugador y sus stats
    private GameObject player;

    // Variable para guardar la posición inicial
    private Vector3 initialPosition;
    Vector3 target;

    // Animador y cuerpo cinemático con la rotación en Z congelada
    private Animator anim;
    private Rigidbody2D rb2d;


    // Variables relacionadas con el ataque
    [Tooltip("Prefab de la roca que se disparará")]
    public GameObject rockPrefab;
    [Tooltip("Velocidad de ataque (segundos entre ataques)")]
    public float attackSpeed = 2f;
    bool attacking;


    void Start()
    {
        roundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<RoundController>();
        gen = GameObject.FindGameObjectWithTag("Enemies").GetComponent<EnemyGen>();

        soundController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();

        enemyStats = GetComponent<EnemyStats>();

        round = roundController.getRound();

        // initializate stats enemigo
        speed = enemyStats.Speed;
        hp = enemyStats.Hp +  (round / 10);
        attack = enemyStats.Attack;
        visionRadius = enemyStats.Vision;
        attackRadius = enemyStats.RadiousAttk*10;


        // Recuperamos al jugador gracias al Tag
        player = GameObject.FindGameObjectWithTag("Player");



        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();


    }

    void Update()
    {
        if (player != null)
        {
            // Guardamos nuestra posición inicial
            initialPosition = transform.position;
            // Por defecto nuestro target siempre será nuestra posición actual
            target = initialPosition;

            // Comprobamos un Raycast del enemigo hasta el jugador
            RaycastHit2D hit = Physics2D.Raycast(
                transform.position,
                player.transform.position - transform.position,
                visionRadius,
                1 << LayerMask.NameToLayer("Default")
            // Poner el propio Enemy en una layer distinta a Default para evitar el raycast
            // También poner al objeto Attack y al Prefab Slash una Layer Attack 
            // Sino los detectará como entorno y se mueve atrás al hacer ataques
            );

            // Aquí podemos debugear el Raycast
            Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
            Debug.DrawRay(transform.position, forward, Color.red);

            // Si el Raycast encuentra al jugador lo ponemos de target
            if (hit.collider != null)
            {
                if (hit.collider.tag == "Player")
                {
                    target = player.transform.position;
                }
            }

            // Calculamos la distancia y dirección actual hasta el target
            float distance = Vector3.Distance(target, transform.position);
            Vector3 dir = (target - transform.position).normalized;


            // Si es el enemigo ya está en rango de ataque nos paramos y le atacamos
            if (target != initialPosition && distance < attackRadius)
            {
                anim.SetFloat("moveX", dir.x);
                anim.SetFloat("moveY", dir.y);
                anim.Play("Tree_Walk", -1, 0);  // Congela la animación de andar

                if (!attacking) StartCoroutine(Attack(attackSpeed));


            }
            // En caso contrario nos movemos hacia él
            else
            {
                rb2d.MovePosition(transform.position + dir * speed * Time.deltaTime);
                anim.speed = 1;
                anim.SetFloat("moveX", dir.x);
                anim.SetFloat("moveY", dir.y);

                // Al movernos establecemos la animación de movimiento
                anim.SetBool("inRange", true);
            }

            // Una última comprobación para evitar bugs forzando la posición inicial
            if (target == initialPosition && distance < 0.02f)
            {
                transform.position = initialPosition;
                // Y cambiamos la animación de nuevo a Idle
                anim.SetBool("inRange", false);
            }

            // Y un debug optativo con una línea hasta el target
            Debug.DrawLine(transform.position, target, Color.green);

        }
    }

    // Podemos dibujar el radio de visión y ataque sobre la escena dibujando una esfera
    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
        Gizmos.DrawWireSphere(transform.position, attackRadius);

    }


    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.SendMessage("attacked", attack);
        }
    }

    public void attacked(float attack)
    {
        hp -= attack;
        if (hp <= 0) { Destroy(gameObject); }
    }

    IEnumerator Attack(float seconds)
    {
        attacking = true;
        // Si tenemos objetivo y el prefab es correcto creamos la roca
        if (target != initialPosition && rockPrefab != null)
        {
            Instantiate(rockPrefab, transform.position, transform.rotation);
            // Esperamos los segundos de turno antes de hacer otro ataque
            yield return new WaitForSeconds(seconds);
        }
        attacking = false;
    }

    private void OnDestroy()
    {
        if (Random.Range(0, 100) <= 30)
        {
            int prob = Random.Range(0, 100);
            if (prob <= 50) Instantiate(coin, transform.position, Quaternion.identity);
            else Instantiate(heart, transform.position, Quaternion.identity);
        }
        gen.SendMessage("killEnemy");
        soundController.SendMessage("playEnemyDeath");
    }

}
