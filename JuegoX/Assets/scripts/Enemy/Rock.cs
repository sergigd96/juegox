﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour {

    [Tooltip("Velocidad de movimiento")]
    public float speed;

    private float attack = 1f;

    private GameObject player;
    private Rigidbody2D rb2d; 
    private Vector3 target, dir; 

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        rb2d = GetComponent<Rigidbody2D>();
        speed = 4.5f;
        // Cogemos la posición del player y su dirección normalizada
        if (player != null)
        {
            target = player.transform.position;
            dir = (target - transform.position).normalized;
        }
    }

    void FixedUpdate()
    {
        // Si hay un objetivo movemos la roca hacia su posición
        if (target != Vector3.zero)
        {
            rb2d.MovePosition(transform.position + (dir * speed) * Time.deltaTime);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        // Si chocamos contra el jugador o un ataque la borramos
        if (col.transform.tag == "Player")
        {
            col.SendMessage("attacked", attack);
            Destroy(gameObject);
        }
    }

    void OnBecameInvisible()
    {
        // Si se sale de la pantalla borramos la roca
        Destroy(gameObject);
    }
}
