﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable : MonoBehaviour
{

    // Variable para guardar el nombre del estado de destruccion
    public string destroyState;
    // Variable con los segundos a esperar antes de desactivar la colisión
    public float timeForDisable;
    // Prefabs de coins i hearts
    public GameObject coin;
    public GameObject heart;


    // Animador para controlar la animación
    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Detectamos la colisión con una corrutina
    IEnumerator OnTriggerEnter2D(Collider2D col)
    {
        // Si es un ataque
        if (col.tag == "Attack")
        {
            // Reproducimos la animación de destrucción y esperamos
            anim.Play(destroyState);
            yield return new WaitForSeconds(timeForDisable);

            // Pasados los segundos de espera desactivamos los colliders 2D
            foreach (Collider2D c in GetComponents<Collider2D>())
            {
                c.enabled = false;
            }

        }

    }

    void Update()
    {

        // "Destruir" el objeto al finalizar la animación de destrucción
        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);

        if (stateInfo.IsName(destroyState) && stateInfo.normalizedTime >= 1)
        {
            Destroy(gameObject);
        }
    }

    void OnDestroy()
    {
        if(Random.Range(0, 100) <= 35)
        {
            int prob = Random.Range(0, 100);

            if (prob <= 50)
                Instantiate(coin, transform.position, Quaternion.identity);
            else
                Instantiate(heart, transform.position, Quaternion.identity);
        }
    }

}
