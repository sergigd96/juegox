﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlaceName : MonoBehaviour {

    private MusicController musicController;
    public Animator anim;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        musicController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MusicController>();
	}
	
	public IEnumerator ShowPlaces(string text)
    {
        anim.Play("Place_Show");
        // Sombra
        transform.GetChild(0).GetComponent<Text>().text = text;
        // Texto
        transform.GetChild(1).GetComponent<Text>().text = text;

        musicController.ChangeMusic(text);

        yield return new WaitForSeconds(1f);
        anim.Play("Place_FadeOut");

    }


}
